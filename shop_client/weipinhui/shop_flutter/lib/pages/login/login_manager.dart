import 'package:shop/lblbc_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginManager {
  static Future<bool> isLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString(Constants.spKeyToken);
    var result = false;
    if (token != null) {
      result = true;
    }
    return result;
  }

  static logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(Constants.spKeyToken);
  }
}
