import request from '../utils/request'
export function queryCategory() {
	return request('', 'shop/categories', 'get').then(resp => resp)
}

export function queryGoodsByCategory(categoryId) {
	return request('', `shop/goods?categoryId=${categoryId}`, 'get').then(resp => resp)
}

export function login(params) {
	return request(params, 'user/login', 'post').then(resp => resp)
}

export function logout(params) {
	return request(params, 'user/logout', 'post').then(resp => resp)
}

export function register(params) {
	return request(params, 'user/register', 'post').then(resp => resp)
}

export function queryGoods(id) {
	return request('', 'shop/goods/'+id, 'get').then(resp => resp)
}

export function search(keyword) {
	return request('', `shop/goodsBySearch?keyword=${keyword}`, 'get').then(resp => resp)
}

export function createOrder(params) {
  return request(params, 'shop/orders', 'post').then(resp => resp)
}

export function createOrderFromCart(params) {
  return request(params, 'shop/ordersFromCart', 'post').then(resp => resp)
}

export function queryOrderList(status) {
	return request('', `shop/orders?orderStatus=${status}`, 'get').then(resp => resp)
}

export function queryOrder(orderId) {
	return request('', `shop/orders/${orderId}`, 'get').then(resp => resp)
}

export function delOrder(orderId) {
	return request('', `shop/orders/${orderId}`, 'delete').then(resp => resp)
}

export function addToCart(params) {
	return request(params, 'shop/cart', 'post').then(resp => resp)
}

export function modifyCart(id,params) {
	return request(params, `shop/cart/${id}`, 'put').then(resp => resp)
}

export function queryCart(params) {
  return request('', 'shop/cart', 'get').then(resp => resp)
}

export function delCartItem(id) {
	  const params = {
		quantity: 0
	  }
	return request('', `shop/cart/${id}`, 'delete').then(resp => resp)
}

export function queryByCartItemIds(cartIds) {
	return request('', `shop/cart/${cartIds}`, 'get').then(resp => resp)
}

export function addAddress(params) {
	return request(params, 'shop/addrs', 'post').then(resp => resp)
}

export function modifyAddress(params) {
  return request(params, 'shop/addrs', 'put').then(resp => resp)
}

export function delAddress(id) {
  return request('', `shop/addrs/${id}`, 'delete').then(resp => resp)
}

export function queryDefaultAddress() {
	return request('', 'shop/addr_default', 'get').then(resp => resp)
}

export function queryAddressList() {
	return request('', 'shop/addrs', 'get').then(resp => resp)
}

export function queryAddressDetail(id) {
  return request('', `shop/addrs/${id}`, 'get').then(resp => resp)
}