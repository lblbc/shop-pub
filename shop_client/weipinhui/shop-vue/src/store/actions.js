import { queryCart } from '../service/axios-services'

export default {
  async updateCart(ctx) {
    const { data } = await queryCart()
    ctx.commit('changeCartItemCount', {
      count: data.length || 0
    })
  }
}
