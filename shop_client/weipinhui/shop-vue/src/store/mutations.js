export default {
  changeCartItemCount (state, payload) {
    state.cartItemCount = payload.count
  }
}
