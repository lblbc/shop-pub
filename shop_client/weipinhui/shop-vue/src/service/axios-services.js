/**
 * 厦门大学计算机专业 | 前华为工程师
 * 专注《零基础学编程系列》  http://lblbc.cn/blog
 * 包含：Java | 安卓 | 前端 | Flutter | iOS | 小程序 | 鸿蒙
 * 公众号：蓝不蓝编程
 */
import axios from '../utils/axios'

export function queryCategory() {
  return axios.get('/shop/categories');
}

export function queryGoodsByCategory(categoryId) {
  return axios.get('/shop/goods?categoryId='+categoryId);
}

export function login(params) {
  return axios.post('/user/login', params);
}

export function logout() {
  return axios.post('/user/logout')
}

export function register(params) {
  return axios.post('/user/register', params);
}

export function queryGoods(id) {
  return axios.get(`/shop/goods/${id}`);
}

export function getCategory() {
  return axios.get('/categories');
}

export function search(id) {
  return axios.get(`/shop/goodsBySearch?keyword=${id}`);
}

export function createOrder(params) {
  return axios.post('/saveOrder', params);
}

export function createOrderFromCart(params) {
  return axios.post('/shop/ordersFromCart', params);
}

export function getOrderList(status) {
  return axios.get(`/shop/orders?orderStatus=${status}`);
}

export function queryOrder(orderId) {
  return axios.get(`/shop/orders/${orderId}`);
}

export function delOrder(orderId) {
  return axios.delete(`/shop/orders/${orderId}`)
}

export function addToCart(params) {
  return axios.post('/shop/cart', params);
}

export function modifyCart(id,params) {
  return axios.put(`/shop/cart/${id}`,params);
}

export function queryCart(params) {
  return axios.get('/shop/cart', { params });
}

export function deleteCartItem(id) {
  const params = {
    quantity: 0
  }
  return axios.put(`/shop/cart/${id}`,{params});
}

export function getByCartItemIds(cartIds) {
  return axios.get(`/shop/cart/${cartIds}`);
}

export function addAddress(params) {
  return axios.post('/shop/addrs', params);
}

export function modifyAddress(params) {
  return axios.put('/shop/addrs', params);
}

export function deleteAddress(id) {
  return axios.delete(`/shop/addrs/${id}`);
}

export function getDefaultAddress() {
  return axios.get('/shop/addr_default');
}

export function getAddressList() {
  return axios.get('/shop/addrs')
}

export function getAddressDetail(id) {
  return axios.get(`/shop/addrs/${id}`)
}


