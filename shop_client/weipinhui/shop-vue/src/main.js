import { createApp } from 'vue'
import { ActionBar, ActionBarIcon, ActionBarButton, Divider, Popup, Overlay, Loading, Dialog, Cell, CellGroup, SwipeCell, Form, AddressList, AddressEdit, Field, Icon, Stepper, Card, Checkbox, CheckboxGroup, Button, PullRefresh, List, Tab, Tabs, SubmitBar, Toast, Skeleton } from 'vant'
import router from './router'
import store from './store'
import App from './App.vue'
import 'lib-flexible/flexible'
import 'vant/lib/index.css'; 

const app = createApp(App)

app.use(ActionBarButton)
  .use(ActionBarIcon)
  .use(ActionBar)
  .use(Divider)
  .use(Popup)
  .use(Overlay)
  .use(Loading)
  .use(Cell)
  .use(SwipeCell)
  .use(CellGroup)
  .use(Field)
  .use(Form)
  .use(AddressList)
  .use(AddressEdit)
  .use(Icon)
  .use(Stepper)
  .use(Card)
  .use(Button)
  .use(PullRefresh)
  .use(List)
  .use(Tab)
  .use(Tabs)
  .use(SubmitBar)
  .use(Checkbox)
  .use(CheckboxGroup)
  .use(Skeleton)
  .use(Dialog)
  .use(Toast)

app.use(store)
app.use(router)

app.mount('#app')